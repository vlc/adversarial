This is a meta-note about this repository more than about the code
itself (that's further down the directory paths).

Since the VLC public repo only covers the media player and other
primarily graphical interfaces, there is no public access to the
libvlc bindings repo.  In particular this one:

git://git.videolan.org/vlc/bindings/python.git

As a consequence a work-around was required.  So the master branch for
the adversarial fork contains no updates.  The libvlc-bindings branch
created the bindings directory as a subdirectory of the master VLC
branch, this file and the submodule data for the python/ subdirectory.

To get to the python/ subdirectory and submodule you must switch to
the libvlc-bindings branch and then switch to the bindings/python/
directory.  At this point you will find yourself back in a master
branch, but now the master branch for that submodule, NOT the master
branch of VLC itself.

My updates are in the agnostic-mod branch (so named because the code
is intended to be platform, CPython version and VLC version agnostic).

If there is any trouble accessing the submodule, I'm mirroring my
patches and updates here:

https://github.com/adversary-org/python-vlc

It is not a full repo of all the VLC or libvlc code, but it will have
all of mine and recreate directory structures where necessary.  In the
worst case scenario of this submodule not behaving as intended, grab
the Python Bindings repo and then my github one and merge the two (the
vlc/bindings/python/ directories should match and add my agnostic
directory to it).  Kinda sucky, but will have to do until I'm sure the
submodules are fine (not likely as it takes a lot more effort to
retrieve as well as update) or until the VLC team decide to include
the libvlc stuff in the public repo.

